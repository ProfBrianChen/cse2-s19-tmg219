/*
Name: Taylor Gomez
Date: 2/1/19
CSE 002 Cyclometer - print the number of minutes for each trip, print the number of counts for each trip, 
print the distance of each trip in miles, print the distance for the two trips combined given input data
*/
public class Cyclometer { //class 
    	// main method 
   	public static void main(String[] args) {
    //declare input data variables
      double secsTrip1=480.0;  //time elapsed in seconds for trip 1
      double secsTrip2=3220.0;  //time elapsed in seconds for trip 2
		  int countsTrip1=1561;  //number of rotations of the front wheel for trip 1
		  int countsTrip2=9037; //number of rotations of the front wheel for trip 2
     //declare intermediate variables and output data variables
      double wheelDiameter=27.0;  //diameter of front wheel 
  	  double PI=3.14159; //value of pi
    	int feetPerMile=5280;  //conversation rate for feet to mile
  	  int inchesPerFoot=12;   //conversation rate for inches to foot
  	  double secondsPerMinute=60.0;  //conversation rate for seconds to minutes
      double distanceTrip1; //variable for distance of trip 1
      double distanceTrip2; //variable for distance of trip 2
      double totalDistance; //variable for toital distance of both trips
      //print values of stored variables 
      System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had " + countsTrip1+" counts."); //prints input data for trip 1
	    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+ " counts."); //prints input data for trip 2
      //calculate distance of both trips in inches and convert to miles
      distanceTrip1=countsTrip1*wheelDiameter*PI; //distance in inches for trip 1 - (for each count, a rotation of the wheel travels the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; //converts distance from inches to miles for trip 1
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance in inches for trip 2 - (for each count, a rotation of the wheel travels the diameter in inches times PI)
	    totalDistance=distanceTrip1+distanceTrip2; //converts distance from inches to miles for trip 2
	    //Print the output data
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //print distance of trip 1 in miles
	    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //print distance of trip 2 in miles
	    System.out.println("The total distance was "+totalDistance+" miles"); //print distance of total distance in miles
	}  //end of main method   
} //end of class
