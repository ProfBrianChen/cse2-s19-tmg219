/*
Name: Taylor Gomez
Date: 3/22/19
CSE 002 Lab 06 Methods - Declaring and calling methods to create a paragraph
*/
import java.util.Scanner; //import scanner 
import java.util.Random; //import random generator
public class Methods { 
  public static String adjectiveGenerator() { //method to generate adjective
    Random randomGenerator = new Random(); //create random number generator
    int randomInt1 = randomGenerator.nextInt(10); //generate random number less than 10 and assign to variable 
    String adjective=""; //declare adjective string variable
    switch (randomInt1) { //switch statement to determine adjective
      case 0: //if random number is 0
        adjective="happy"; //assign value to variable
        break; //leave case
      case 1:  //if random number is 1
        adjective="sad"; //assign value to variable
        break; //leave case
      case 2: //if random number is 2
        adjective="dazzling"; //assign value to variable
        break; //leave case 
      case 3: //if random number is 3
        adjective="lazy"; //assign value to variable
        break; //leave case
      case 4: //if random number is 4
        adjective="magnificent"; //assign value to variable
        break; //leave case
      case 5: //if random number is 5
        adjective="little"; //assign value to variable
        break; //leave case
      case 6: //if random number is 6
        adjective="large"; //assign value to variable
        break; //leave case
      case 7: //if random number is 7
        adjective="wonderful"; //assign value to variable
        break; //leave case
      case 8: //if random number is 8
        adjective="clumsy"; //assign value to variable
        break; //leave case
      case 9: //if random number is 9
        adjective="polite"; //assign value to variable
        break; //leave case
    }//end switch statement
    return adjective; //return variable to be used when called 
  }//end adjective method
  public static String subjectGenerator() { //method to generate subject 
    Random randomGenerator = new Random(); //create random number generator
    int randomInt2 = randomGenerator.nextInt(10); //generate random number less than 10 and assign to variable 
    String subject=""; //declare subject string variable
    switch (randomInt2) { //switch statement to determine adjective
      case 0: //if random number is 0 
        subject="penguin"; //assign value to variable
        break; //leave case
      case 1: //if random number is 1
        subject="car"; //assign value to variable
        break; //leave case
      case 2: //if random number is 2
        subject="pretzel"; //assign value to variable
        break; //leave case
      case 3: //if random number is 3
        subject="dog"; //assign value to variable
        break; //leave case
      case 4: //if random number is 4
        subject="cat"; //assign value to variable
        break; //leave case
      case 5: //if random number is 5
        subject="sweatshirt";//assign value to variable
        break; //leave case
      case 6: //if random number is 6
        subject="alpaca"; //assign value to variable
        break; //leave case
      case 7: //if random number is 7
        subject="llama"; //assign value to variable
        break; //leave case
      case 8: //if random number is 8
        subject="Packard"; //assign value to variable
        break; //leave case
      case 9: //if random number is 9
        subject="computer"; //assign value to variable
        break; //leave case
    }//end switch statement
    return subject; //return variable to be used when called 
  }//end subject method for subject
  public static String verbGenerator() { //method to generate verb
    Random randomGenerator = new Random(); //create random number generator
    int randomInt3 = randomGenerator.nextInt(10); //generate random number less than 10 and assign to variable 
    String verb=""; //declare verb string variable
    switch (randomInt3) { //switch statement to determine verb
      case 0: //if random number is 0
        verb="admired"; //assign value to variable
        break; //leave case
      case 1: //if random number is 1
        verb="left"; //assign value to variable
        break; //leave case
      case 2: //if random number is 2
        verb="jumped"; //assign value to variable
        break; //leave case
      case 3: //if random number is 3
        verb="avoided"; //assign value to variable
        break; //leave case
      case 4: //if random number is 4
        verb="jumped"; //assign value to variable
        break; //leave case
      case 5: //if random number is 5
        verb="hugged"; //assign value to variable
        break; //leave case
      case 6: //if random number is 6
        verb="discovered"; //assign value to variable
        break; //leave case
      case 7: //if random number is 7
        verb="raced"; //assign value to variable
        break; //leave case
      case 8: //if random number is 8
        verb="stopped"; //assign value to variable
        break; //leave case
      case 9: //if random number is 9
        verb="tripped"; //assign value to variable
        break; //leave case
    }//end switch statement
    return verb; //return variable to be used when called 
  }//end subject method for verb generator
  public static String objectGenerator() { //method to generate object
    Random randomGenerator = new Random(); //create random number generator
    int randomInt4 = randomGenerator.nextInt(10); //generate random number less than 10 and assign to variable 
    String object=""; //declare object string variable
    switch (randomInt4) { //switch statement to determine object
      case 0: //if random number is 0
        object="penguin"; //assign value to variable
        break; //leave case
      case 1: //if random number is 1
        object="car"; //assign value to variable
        break; //leave case
      case 2: //if random number is 2
        object="pretzel"; //assign value to variable
        break; //leave case
      case 3: //if random number is 3
        object="dog"; //assign value to variable
        break; //leave case
      case 4: //if random number is 4
        object="cat"; //assign value to variable
        break; //leave case
      case 5: //if random number is 5
        object="sweatshirt"; //assign value to variable
        break; //leave case
      case 6: //if random number is 6
        object="alpaca"; //assign value to variable
        break; //leave case
      case 7: //if random number is 7
        object="llama"; //assign value to variable
        break; //leave case
      case 8: //if random number is 8
        object="Packard"; //assign value to variable
        break; //leave case
      case 9: //if random number is 9
        object="computer"; //assign value to variable
        break; //leave case
    }//end switch statement
    return object; //return variable to be used when called 
  }//end subject method for object
  public static String thesisSentence (String adjective, String adjective1, String adjective2, String subject, String verb, String object) { //method for thesis sentence
    String thesisSentence="The " + adjective + " " + adjective1 + " " + subject + " " + verb + " the " + adjective2 + " " + object + "."; //declare and assign thesis ssentence variable
    return thesisSentence; //return variable to be used when called 
  }//end thesis sentence method
  public static String supportingSentence (String adjective, String adjective1, String subject, String verb, String object, String object1) { //method for supporting sentences
    String supportingSentence="This " + subject + " was very " + adjective + " to the " + adjective1 + " " + object + "."; //declare and assign supporting sentence variable
    return supportingSentence; //return variable to be used when called 
  }//end supporting sentence method
  public static String conclusionSentence (String subject, String verb, String object) { //method for conclusion sentence
      String conclusionSentence="That " + subject + " " + verb + " its " + object + "."; //declare and assign conclusion sentence variable
      return conclusionSentence; //return variable to be used when called 
  }//end conclusion sentence method
  public static String finalParagraph (String thesis, String subject) { //method for final paragraph
    Random randomGenerator = new Random(); //create random number generator
    String supportingSentence=""; //declare supporting sentence variable
    String realVerb2=verbGenerator(); //declare and assign value to verb variable by random generator 
    String realObject3=objectGenerator(); //declare and assign value to object variable by random generator 
    int randomNumber = randomGenerator.nextInt(9)+1; //declare and assign random number variable by random generator
    for (int i=0; i<randomNumber;i++) { //for loop for number of supporting sentences
      String realAdjective3=adjectiveGenerator(); //declare and assign value to adjective variable by random generator 
      String realAdjective4=adjectiveGenerator(); //declare and assign value to adjective variable by random generator 
      String realVerb1=verbGenerator(); //declare and assign value to verb variable by random generator 
      String realObject1=objectGenerator(); //declare and assign value to object variable by random generator 
      String realObject2=objectGenerator(); //declare and assign value to object variable by random generator 
      supportingSentence+= supportingSentence(realAdjective3,realAdjective4,subject,realVerb1,realObject1,realObject2) + " \n"; //assign value to support sentence variable
     } //end for loop for number of supporting sentences
    String conclusionSentence=conclusionSentence(subject, realVerb2,realObject3); //declare and assign value to conclusion sentence variable
    String finalParagraph=thesis + " \n" + supportingSentence + conclusionSentence;  //declare and assign value to final paragraph variable
    return finalParagraph; //return variable to be used when called
  }//end final paragraph method
  //main method 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    Random randomGenerator = new Random(); //create random number generator
    String realAdjective=adjectiveGenerator(); //declare and assign value to adjective variable by random generator 
    String realAdjective1=adjectiveGenerator(); //declare and assign value to adjective variable by random generator 
    String realAdjective2=adjectiveGenerator(); //declare and assign value to adjective variable by random generator 
    String realSubject=subjectGenerator(); //declare and assign value to subject variable by random generator 
    String realVerb=verbGenerator(); //declare and assign value to verb variable by random generator 
    String realObject=objectGenerator(); //declare and assign value to object variable by random generator 
    String thesisSentence=thesisSentence(realAdjective,realAdjective1,realAdjective2,realSubject,realVerb,realObject); //declare and assign value to thesis statement variable
    System.out.println(thesisSentence); //print out thesis statement 
    System.out.println("Would you like a supporting paragraph? Please respond with yes or no."); //ask user if they want rest of paragraph
    String newSentence = myScanner.next(); //declare and assign value to variable through scanner (user input)
      if (newSentence.equalsIgnoreCase("yes")) { //if statement if user wants rest of paragraph (says yes)
        String finalParagraph=finalParagraph(thesisSentence, realSubject); //declare and assign final paragraph variable by calling method
        System.out.println(finalParagraph); //print out variable
      } //end if statement if user says yes
      else if (newSentence.equalsIgnoreCase("no")) { //else if statement if user says no
        System.out.println("The end. That's the end of the story."); //print out statement to show user it's the end of story
      } //end else if statement if user says no
  }//end main method
} //end class