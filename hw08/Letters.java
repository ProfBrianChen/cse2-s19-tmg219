/*
Name: Taylor Gomez
Date: 4/9/19
CSE 002 HW 08 Letters - Create an array of arbitrary size with random char values (uppercase and lowercase)
and separate into two different arrays using two different methods (A-M and N-Z) and return both as output
*/
import java.util.Arrays;
import java.util.Random;
public class Letters {
  public static char[] getAtoM(char[] list) { //method to find a to m in original array 
    char[] arrayAtoM = new char[15]; //declare char array for a to m values
    int j=0; //declare variable for spot in new array
    for (int i=0; i<list.length; i++) { //for loop to iterate through input array
      if ((list[i]>=(char)(65)&&(list[i]<=(char)(77))) || (list[i]>=(char)(97)&&(list[i]<=(char)(109)))) {//if loop to catch a-m values in array
          arrayAtoM[j]=list[i]; //put value in original array into new array
          j++; //add one to j counter
      }//end if loop 
      else { //else statement
        continue; //continue through loop
      }//end else statement 
    }//end for loop
    return arrayAtoM; //return new array values 
  }//end a to m method
  
  public static char[] getNtoZ(char[] list) { //method to find n to z in original array 
    char[] arrayNtoZ = new char[15]; //declare char array for n to z values
    int j=0; //declare variable for spot in new array
    for (int i=0; i<list.length; i++) { //for loop to iterate through input array
      if ((list[i]>=(char)(78)&&(list[i]<=(char)(90))) || (list[i]>=(char)(110)&&(list[i]<=(char)(122)))) {//if loop to catch n-z values in array
          arrayNtoZ[j]=list[i]; //put value in original array into new array
          j++; //add one to j counter
      }//end if loop 
      else { //else statement
        continue; //continue through loop
      }//end else statement 
    }//end for loop
    return arrayNtoZ; //return new array values 
  }//end a to m method
  
  public static void main (String[] args) {
    Random randomGenerator = new Random(); //create random number generator
    char[] randomArray = new char[15]; //declare and allocate array with number of spaces available equal to that of the random number generated
    int letter; //declare variable for letter
    System.out.print("Random character array: "); //print out line
    for (int i=0; i<randomArray.length; i++) { //for loop to iterate through array
      letter = randomGenerator.nextInt(58)+65; //generate random number between 65 and 122
      while (letter>= 91 && letter<=96) { //if random number is between 91 and 96 inclusive, run loop (because it doesn't correspond to a lowercase or uppercase letter in ASCII table)
        letter=randomGenerator.nextInt(58)+65; //generate a new number 
      } //end while loop 
      randomArray[i]=(char)(letter); //find char value of number and put in array 
      System.out.print(randomArray[i]); //print out each value of array 
    }//end for loop
    System.out.println(); //move to next line
    char[] aTomArray=getAtoM(randomArray); //create new array for a to m and call method 
    System.out.print("AtoM characters: "); //print line
    for (int i=0; i<aTomArray.length; i++) { //for loop to iterate through array
      System.out.print(aTomArray[i]); //print out each value of array
    } //end for loop
    System.out.println(); //move to next line
    char[] nTozArray=getNtoZ(randomArray); //crate new array for n to z and call method
    System.out.print("NtoZ characters: "); //print line
    for (int i=0; i<nTozArray.length; i++) { //for loop to iterate through array 
      System.out.print(nTozArray[i]); //print out each value of array
    } //end for loop 
    System.out.println(); //move to next line
  }//end main method
} //end class