/*
Name: Taylor Gomez
Date: 4/9/19
CSE 002 HW 08 Letters - Create an array of arbitrary size with random char values (uppercase and lowercase)
and separate into two different arrays using two different methods (A-M and N-Z) and return both as output
*/
import java.util.Arrays; //import arrays
import java.util.Random; //import random generator scanner
import java.util.Scanner; //import scanner 
public class PlayLottery {
  public static int userCheck (Scanner myScanner) { //method to check user input
    int userInput; //declare userInput variable
    while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  String junkWord = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  userInput = myScanner.nextInt(); //assign correct input to length variable
      if (userInput>=0 && userInput<=59){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter an integer between 0 and 59. Please try again."); //ask user for new input 
    }//end while loop
    return userInput; //return variable
  }//end user check method
  
  public static int[] numbersPicked() { //method for random generated array
    Random randomGenerator = new Random(); //create random number generator
    int [] randomArray = new int[5]; //declare array of length 5
    int counter=1; //declare counter
    randomArray[0]=randomGenerator.nextInt(60); //set first space in array to randomly generated number between 0 and 59
    for (int i=1; i<randomArray.length; i++) { //for loop to iterate through array
      randomArray[i]=randomGenerator.nextInt(60); //at ith position, assign random generated number
      counter++; //add one to counter
      for (int j=0; j<counter; j++) { //for loop to iterate through array
        if ((randomArray[i]==randomArray[j]) && (i!=j)) { //if statement to check for duplicates
          randomArray[i]=randomGenerator.nextInt(60); //at ith position, assign random generated number
        } //end if statement
      } //end j for loop  
    } //end i for loop
    return randomArray; //return random array
  }
  
  public static boolean userWins(int[] user, int[] random) { //method to compare arrays
    boolean result = true; //declare boolean and set to true
    for (int i = 0; i<5; i++) { //for loop to iterate through array
      if (user[i]==random[i]) { //if statement to compare ith position of both arrays and they are equal
        continue; //continue through loop
      }//end if statement 
      else { //else statement if values at ith position are not equal
        result = false; //set boolean to false
        break; //break out of loop
      }//end else statement
    }//end for loop
    return result; //return result 
  }//end method comparing arrays

  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    Random randomGenerator = new Random(); //create random number generator
    int[] userArray = new int[5]; //declare and allocate array with 5 spaces
    System.out.print("Enter 5 numbers between 0 and 59: "); //print line
    for (int i=0; i<userArray.length; i++) {//for loop to iterate through array
      userArray[i] = userCheck(myScanner); //fill in array with user input using check method
    }//end for loop
    int [] randomArray = numbersPicked(); //declare random array 
    boolean result=userWins(userArray,randomArray); //decalre boolean and call method 
    System.out.print("The user's numbers are: "); //print line
    for (int i=0; i<userArray.length; i++) {//for loop to iterate through array
      System.out.print(userArray[i] + " "); //print out each value of array
    }//end for loop
    System.out.println(); //move to next line
    System.out.print("The winning numbers are: "); //print line
    for (int i=0; i<userArray.length; i++) {//for loop to iterate through array
      System.out.print(randomArray[i] + " "); //print out each value of array
    }//end for loop
    System.out.println(); //move ot next line
    if (result) { //if statement if boolean is true
      System.out.println("You win"); //print line
    } //end if statement
    else { //else statement if boolean is false
      System.out.println("You lose"); //print line
    } //end else statement
  }//end main method
} //end class