/*
Name: Taylor Gomez
Date: 2/8/19
CSE 002 Check - Uses the Scanner class for user to input the original cost of the check, the percentage tip they wish to pay, and the number of ways the check will be split.
Determine how much each person in the group needs to spend in order to pay the check.
*/
import java.util.Scanner; //import scanner class
public class Check { //class 
    	// main method 
   	public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
      //receive inputs from user
      System.out.print("Enter the original cost of the check in the form xx.xx: "); //ask user to input original cost of check with two decimals
      double checkCost = myScanner.nextDouble(); //user's input is placed in declared variable (cost of check)
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):  "); //ask user to input percentage tip for check
      double tipPercent = myScanner.nextDouble(); //user's input is place in declared variable (percentage tip on check)
      tipPercent /= 100; //convert tip from whole number percentage to decimal 
      System.out.print("Enter the number of people who went out to dinner: "); //ask user to input # of ppl at dinner in order to split the check
      int numPeople = myScanner.nextInt(); //user's input is placed in a declared variable (number of ppl at dinner)
      
      //declare intermediate and output variables and assign value 
      double totalCost = checkCost * (1+tipPercent); //total cost of check including tip
      double costPerPerson = totalCost / numPeople; //cost per person by dividing total cost by # of ppl
      int dollars = (int)costPerPerson; //whole dollar amount of cost per person (drop decimal fraction)
      int dimes=(int)(costPerPerson * 10) % 10; //tenths amount of cost (one to right of decimal) --> % returns remainder 
      int pennies=(int)(costPerPerson * 100) % 10; //thousanths amount of cost (two to right of decimal) --> % returns remainder 

      //print cost per person in terms of dollar, dimes, pennies
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    } //end main method
} //end class
