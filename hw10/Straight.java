/*
Name: Taylor Gomez
Date: 4/30/19
CSE 002 HW10 - Search a poker hand and determine if hand is a straight 
*/
import java.util.Arrays; //import arrays
import java.util.Random; //import random generator 
import java.util.Scanner; //import scanner 
public class Straight {
  
  public static int [] shuffle () { //generate shuffled deck of cards
    int [] cardDeck = new int [52]; //create array of length 52
    for (int i=0; i<cardDeck.length; i++) {
      cardDeck[i]=i; //assign value of i to each spot
    }//end for loop 
    for (int i =0; i<cardDeck.length; i++) { //for loop to shuffle array and iterate through each spot
      int randomSpot = (int) (cardDeck.length * Math.random() );; //choose random spot in array
      int tempSpot = cardDeck[randomSpot]; //assign value of random spot to temporary variable
      cardDeck[randomSpot]=cardDeck[i]; //assign value of ith spot in array to random spot in array
      cardDeck[i]=tempSpot; //assign value of random spot (from temporary variable) to ith spot in array
    }//end for loop
    return cardDeck; //return variable to be called in other methods
  }//end shuffle method
  
  public static int [] pickHand () {
    int [] deck = shuffle(); //call shuffle method
    int [] hand = new int[5]; //create array of length 5 (to represent a hand)
    for (int i=0; i<hand.length; i++) { //for loop to iterate through array
         hand[i] = deck[i]; //assign first five cards of deck to spots in array
       }//end for loop
       return hand; //return variable to be called in other methods
     }//end pickHand method
    
    public static int findCard (int [] originalHand, int k) {
      int [] hand = new int [originalHand.length]; //create new variable to avoid pass by reference
      for (int z=0; z<hand.length; z++) {
        hand[z]=originalHand[z]; //copy arrays
      }//end for loop 
      if (k>5 || k<0) {
        return -1; //return value
      }//end if loop 
      int min=999; //create minimum variable
      int index=-1; //create variable
      int value; //create variable
      for (int i=0; i<k; i++) {
        for (int j=0; j<hand.length; j++) {
          value=(hand[j]%13);//determine value of cards
          if (min>value) { //if new minimum exists
            min = value; //set variable equal to value of another variable
            index = j; //set variable equal to value of another variable
          }//end if statement
        }//end j loop
        if (i==(k-1)){
          break; //break out of loop 
        }
          hand[index]=999; //set value equal to 999 (high number)
          min=999;
          index=-1; 
      }//end i for loop 
      return index; 
    }//end find card method
 
  public static boolean search(int[] hand) {
    int place1 = findCard(hand,1); //use find card method to find minimum value
    for (int i=1; i<hand.length; i++) {
     int place2=findCard(hand,i+1); //call find card method
     if (hand[place2]!=(hand[place1]+1)) {
       return false; //return variable to be called later
     }//end if statement
      place1=place2; //reset place variable 
    }//end for loop
    return true; //return variable to be called later
  }//end search method
  
  public static void main (String[] args) { //main method
    int counter=0; //declare counter variable 
    double straightCounter=0; //declare counter variable
    boolean foundStraight; //declare boolean variable
    int[] pokerHand = new int[5]; //declare poker hand
    while (counter<1000000) {
      pokerHand=pickHand(); //call pick Hand method
      foundStraight=search(pokerHand); //determine if straight is present by calling search method
      if (foundStraight==true){
        straightCounter++; //add one to counter if straight foundStraight
      }//end if statement
      counter++; //add one to counter
    }//end while loop
    double percentage=((straightCounter/1000000.00)*100.00); //determine percentage of straights found
    System.out.println("The probability from this simulation is: " + percentage + "%"); //print line
    if (percentage>0.3925) {
      System.out.println("This is greater than the 0.3925% probability of drawing a straight"); //print line
    }//end if statement
    else {
      System.out.println("This is not greater than the 0.3925% probability of drawing a straight"); //print line
    }//end else statement
  }//end main method
} //end class