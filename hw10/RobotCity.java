/*
Name: Taylor Gomez
Date: 4/30/19
CSE 002 HW10 - Use a 2D array to represent a city and see where the "robots" invade as they move from block to block
*/
import java.util.Arrays; //import arrays
import java.util.Random; //import random generator 
import java.lang.Math; //import math
public class RobotCity {
  public static int[][] buildCity () {
    Random randomGenerator = new Random(); //create random number generator
    int dim1 = randomGenerator.nextInt(6)+10; //create random number between 10 and 15
    int dim2 = randomGenerator.nextInt(6)+10; //create random number between 10 and 15
    int[][] city = new int[dim1][dim2]; //declare array of city of size dim1 x dim2
    for (int i=0; i<dim1; i++) { //for loop to iterate through array
        for (int j=0; j<dim2; j++) { //for loop to iterate through array
          int random = randomGenerator.nextInt(900)+100; //create random variable between 100 and 999
          city[i][j] = random; //add random value to each spot in array 
        }//end j for loop
      }//end i for loop
    return city; //return variable to be called in other methods
  }//end build city method
  
  public static void display(int[][] city) {
    for (int i=0; i<city.length; i++) { //for loop to iterate through array
      for (int j=0; j<city[0].length; j++) { //for loop to iterate through array
        System.out.printf("%4d ", city[i][j]); //use printf to print array in grid format
      }//end for j loop
      System.out.println(); //move to next line
    }//end for i loop
  }//end display method
  
  public static int[][] invade (int[][] city, int k) {
    Random randomGenerator = new Random(); //create random number generator
    int counter = 0; //declare counter variable
    int value; //declare value variable
    while (counter<=k) {
      int ran1 = randomGenerator.nextInt(city.length); //declare random variable between 0 and length
      int ran2 = randomGenerator.nextInt(city[0].length); //declare random variable between 0 and length 
      if (city[ran1][ran2] > 0) { //if loop to determine if robot has invaded spot
        value = city[ran1][ran2]; //set value of int variable to value of array 
        city[ran1][ran2] = -value; //set value of random location to negative of its value to signify robot invasion
        counter++; //add one to counter only if robot is not already in spot
      } //end if loop 
    }//end while loop
    return city; //return variable to be called in other methods
  }//end invade method
  
  public static int [][] update (int[][] city) {
    int value; //declare variable
    for (int i=0; i<city.length; i++) {
      for (int j=city[0].length-1; j>0; j--) {
        value=city[i][j]; //set variable equal to value at array spot
        if (city[i][j-1]<0) {
          city[i][j]=-Math.abs(value); //change value of array spot if robot in previous spot (left)
        }//end if statement
        else if (city[i][j-1]>0) {
          city[i][j]=Math.abs(value); //change value of array spot if no robot previously
        }//end else if statement
      }//end j for loop
      city[i][0] = Math.abs(city[i][0]); //set first column of array equal to positive value
    }//end i for loop
    return city; //variable to be called in other method
  }//end update method
  
  //In your main method, then update and display in a loop 5 times.
  public static void main (String[] args) { //main method
    Random randomGenerator = new Random(); //create random number generator
    int [][] cityArray = buildCity(); //build city
    int area = cityArray.length * cityArray[0].length; //determine max number of robots
    int randomRobots = randomGenerator.nextInt(area); //create random number between 0 and number of total array spaces
    System.out.println("This is the original city:"); //print statement
    display(cityArray); //call display method for original city
    int [][] invadedCity = invade(cityArray, randomRobots); //call invade method 
    System.out.println("This is the invaded city with " + randomRobots + " invading robots: "); //print statement
    display(invadedCity); //call display method
    int counter=1; //declare counter variable
    while (counter<6) {
      System.out.println("This is the updated invaded city: (" + counter + ")"); //print statement
      invadedCity = update(invadedCity); //call update method
      display(invadedCity); //call display method
      counter++; //add one to counter
    }//end while loop 
  }//end main method
}//end class