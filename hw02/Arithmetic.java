/*
Name: Taylor Gomez
Date: 2/5/19
CSE 002 Arithmetic - compute the cost of the items you bought, including the PA sales tax of 6% 
*/
public class Arithmetic { //class 
    	// main method 
   	public static void main(String[] args) {
      //declare input data variables
      int numPants = 3; //Number of pairs of pants
      double pantsPrice = 34.98; //Cost per pair of pants
      int numShirts = 2; //Number of sweatshirts
      double shirtPrice = 24.99; //Cost per sweatshirt
      int numBelts = 1; //Number of belts
      double beltPrice = 33.99; //cost per belt
      double paSalesTax = 0.06; //PA tax rate
      
      //declare intermediate variables and output data variables and assign values/associated calculation
      double totalCostOfPants=numPants*pantsPrice; //total cost of pants equals number of pants multipled by its price 
      int totalCostOfPantsInt = (int) (totalCostOfPants*100); //convert double to int and multiply by 100
      double totalCostOfShirts=numShirts*shirtPrice; //total cost of sweatshirts equals number of sweatshirts multipled by its price
      int totalCostOfShirtsInt = (int) (totalCostOfShirts*100); //convert double to int and multiply by 100
      double totalCostOfBelts=numBelts*beltPrice; //total cost of belts equals number of belts multipled by its price
      int totalCostOfBeltsInt = (int) (totalCostOfBelts*100); //convert double to int and multiply by 100
      
      double salesTaxOnPants=totalCostOfPants*paSalesTax; //sales tax charge on pants equals total cost of pants multipled by sales tax
      int salesTaxOnPantsInt = (int) (salesTaxOnPants*100); //convert double to int and multiply by 100
      double salesTaxOnShirts=totalCostOfShirts*paSalesTax; //sales tax charge on sweatshirts equals total cost of sweatshirts multipled by sales tax
      int salesTaxOnShirtsInt = (int) (salesTaxOnShirts*100); //convert double to int and multiply by 100
      double salesTaxOnBelts=totalCostOfBelts*paSalesTax; //sales tax charge on belts equals total cost of belts multipled by sales tax
      int salesTaxOnBeltsInt = (int) (salesTaxOnBelts*100); //convert double to int and multiply by 100
      
      //total cost of purchases BEFORE sales tax equals sum of total costs of all items
      double costOfPurchasesNoTax=totalCostOfPants+totalCostOfShirts+totalCostOfBelts; 
      int costOfPurchasesNoTaxInt = (int) (costOfPurchasesNoTax*100); //convert double to int and multiply by 100
      //total sales tax equals sum of total sales tax of all items
      double totalSalesTax=salesTaxOnPants+salesTaxOnShirts+salesTaxOnBelts; 
      int totalSalesTaxInt = (int) (totalSalesTax*100); //convert double to int and multiply by 100
      //total paid for transaction AFTER sales tax equals sum of total cost of purchase and total sales tax
      double totalCostOfTransaction=costOfPurchasesNoTax+totalSalesTax; 
      int totalCostOfTransactionInt = (int) (totalCostOfTransaction*100); //convert double to int and multiply by 100
      
      //Display output data
      System.out.println("The cost for "+numPants+" pant(s) for $"+pantsPrice+" each before tax is $"+(totalCostOfPantsInt/100.0)); //print total cost of pants
      System.out.println("The cost for "+numShirts+" sweatshirt(s) for $"+shirtPrice+" each before tax is $"+(totalCostOfShirtsInt/100.0)); //print total cost of sweatshirts
      System.out.println("The cost for "+numBelts+" belt(s) for $"+beltPrice+" each before tax is $"+(totalCostOfBeltsInt/100.0)); //print total cost of pants
      System.out.println("The total sales tax for pants is $"+(salesTaxOnPantsInt/100.0)); //print total sales tax of pants
      System.out.println("The total sales tax for sweatshirts is $"+(salesTaxOnShirtsInt/100.0)); //print total sales tax of sweatshirts
      System.out.println("The total sales tax for belts is $"+(salesTaxOnBeltsInt/100.0)); //print total sales tax of belts
      System.out.println("The total cost of the purchases before sales tax is $"+(costOfPurchasesNoTaxInt/100.0)); //print total cost of purchase before sales tax
      System.out.println("The total sales tax on the purchase is $"+(totalSalesTaxInt/100.0)); //print total sales tax values
      System.out.println("The total cost of the purchases after sales tax is $"+(totalCostOfTransactionInt/100.0)); //print total cost of purchase after sales tax
	}  //end of main method   
} //end of class