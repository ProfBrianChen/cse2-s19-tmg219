/*
Name: Taylor Gomez
Date: 4/19/19
CSE 002 lab 10 - Print out and operate on row- and column- major matrices represented by two dimensional arrays. 
*/

import java.util.Arrays; //import arrays
import java.util.Random; //import random generator 
import java.util.Scanner; //import scanner 
public class Matrices {
  public static int[][] increasingMatrix(int width, int height, boolean format) {
    int[][] array; //declare array
    int counter=1; //declare counter variable
    int temp=2; //declare temp variable to help with counter variable 
    if (format) { //row-major
      array=new int[height][width]; //declare size of array
      for (int i=0; i<height; i++) {
        for (int j=0; j<width; j++) {
          array[i][j] = counter; //add value to each spot in array 
          counter++; //add one to counter
        }//end if statement
      }//end if statement 
    }//end if statement
    else { //column-major
      array=new int[width][height]; //declare size of array
      for (int i=0; i<width; i++) {
        for (int j=0; j<height;j++) {
          array[i][j]=counter; //add value to each spot in array
          counter+=width; //add width to counter
        }//end if statement
        counter=temp; //reset counter
        temp++; //add one to temp variable
      }//end if statement
    }//end else statement
    return array; //return
  }//end increasing matrix method
  
  public static void printMatrix(int[][] array, boolean format) {
    if (format) { //row-major
      for (int i=0; i<array.length; i++) { //for loop to iterate through array
        for (int j=0; j<array[0].length; j++) { //for loop to iterate through array
          System.out.printf("%4d ", array[i][j]); //print out specific spot of array
        }//end for loop
      System.out.println(); //move to new line
      }//end for loop 
    }//end if statement
    else { //column-major
      for (int i=0; i<array[0].length; i++) { //for loop to iterate through array
        for (int j=0; j<array.length;j++) { //for loop to iterate through array
          System.out.printf("%4d ", array[j][i]); //print out specific spot of array
        } //end for loop
        System.out.println(); //move to new line
      }//end for loop
    } //end else statement
  }//end print matrix method
  
  public static int [][] translate (int[][] array){
    int height = array[0].length; //find height of array
    int width = array.length; //find width of array
    int [][] arrayRow = new int[height][width];
    for (int i=0; i<height; i++) {
      for (int j=0; j<width; j++){
        arrayRow[i][j] = array[j][i];
      }//end j for loop
    }//end i for loop
    return arrayRow; //return variable to be called in other method
  }//end translate method
  
  public static int [][] addMatrix (int [][] a, boolean formata, int[][] b, boolean formatb) {
    //translate to row matrix if column matrix 
    if (formata == false) {
      a = translate(a); //call translate method
      formata=true; //change boolean variable
    }//end if statement
    else if (formatb == false) {
      b = translate(b); //call translate method
      formatb=true; //change boolean variable
    }//end else if statement
    int [][] newMatrix = new int[a.length][a[0].length]; //create a new matrix for addition of two matrices
    if ((a.length == b.length) && (a[0].length==b[0].length)) {
      for (int i=0; i<newMatrix.length; i++) {
        for (int j=0; j<newMatrix[0].length;j++) {
          newMatrix[i][j] = (a[i][j] + b[i][j]); //add spots in a and b and assign to spot in new matrix
        }//end j for loop
      }//end i for loop
    }//end if statement
     else {
      System.out.println("The arrays cannot be added!"); //print line
      return null; //return null 
    }//end else statement
    return newMatrix; //return variable to be called in other methods
  }//end add matrix method
  
  public static void main (String[] args) { //main method
    Random randomGenerator = new Random(); //create random number generator
    boolean row = true; //declare true boolean variable
    boolean column = false; //declare false boolean variable
    int height1 = randomGenerator.nextInt(10)+1; //create random number between 1 and 10
    int width1 = randomGenerator.nextInt(10)+1; //create random number between 1 and 10
    int height2 = randomGenerator.nextInt(10)+1; //create random number between 1 and 10
    int width2 = randomGenerator.nextInt(10)+1; //create random number between 1 and 10
    int [][] A = increasingMatrix(width1, height1, row); //create matrix A
    System.out.println("This is matrix A:");
    printMatrix(A, row); //call print method
    System.out.println();//move to next line
    int [][] B = increasingMatrix(width1, height1, column); //create matrix B
    System.out.println("This is matrix B:");
    printMatrix(B, column); //call print method
    System.out.println();//move to next line
    int [][] C = increasingMatrix(width2, height2, row); //create matrix C
    System.out.println("This is matrix C:");
    printMatrix(C, row); //call print method
    System.out.println();//move to next line
    int [][] D = addMatrix(A, row, B, column); //call add matrix method
    System.out.println("This is matrix A + matrix B :");
    printMatrix(D, row); //call print method
    System.out.println();//move to next line
    System.out.println("This is matrix A + matrix C :");
    int [][] E = addMatrix(A, row, C, row); //call add matrix method
    if (E!=null) {
      printMatrix(E, row); //call print method
      System.out.println();//move to next line
    }
  }//end main method
}//end class