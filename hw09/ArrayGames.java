/*
Name: Taylor Gomez
Date: 4/12/19
CSE 002 HW 09 - ask user to choose which method to use and run said method using randomly generated array
*/

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class ArrayGames {
  public static int[] generate() {
    Random randomGenerator = new Random(); //create random number generator
    int randomNumber = randomGenerator.nextInt(11)+10; //generate random number between 10 and 20 
    int [] randomArray = new int[randomNumber]; //declare array of random length
    int counter = 0; //declare counter variable
    for (int i=0; i<randomArray.length; i++) {
      randomArray[i]=randomGenerator.nextInt(101); //at ith position, assign random generated number
      counter++; //increase counter by 1
      for (int j=0; j<counter; j++) { //for loop to iterate through array
        if ((randomArray[i]==randomArray[j]) && (i!=j)) { //if statement to check for duplicates
          randomArray[i]=randomGenerator.nextInt(101); //at ith position, assign random generated number
        } //end if statement
      } //end j for loop  
    } //end outer for loop
    return randomArray; //return array to be called
  } //end generate method
  
  public static int[] insert(int[] list1, int[]list2) {
    Random randomGenerator = new Random(); //create random number generator
    int sumLength = list1.length + list2.length; //determine length of new array
    int [] newArray = new int[sumLength]; //create array of new length 
    int randomNumber = randomGenerator.nextInt(list1.length); //choose a random number up to length of first array
    int addSum = randomNumber+list2.length;
    for (int k=0; k<randomNumber; k++) { //for loop to add first array up to randomNumber spot to new array
      newArray[k]=list1[k];
    }//end k loop
    for (int l=randomNumber; l<addSum; l++) { //for loop to add second array to new array
      newArray[l]=list2[(l-randomNumber)]; 
    }//end l for loop
    for (int n=addSum; n<newArray.length; n++) { //for loop to add rest of first array to new array
      newArray[n]=list1[(n-list2.length)];
    }//end n for loop
    return newArray; //return array to be called
  }//end insert method
  
  //shorten method
  public static int[] shorten(int[] list1, int number) {
   // int [] shortenArray;//create array of new length 
    if (number >= list1.length) { //if statement if random number is greater than length of array
      int [] shortenArray = new int[list1.length];
      for (int i=0; i<list1.length; i++) { //for statement to iterate through array
        shortenArray[i] = list1[i]; //copy original array into new array
      }//end for statement
      return shortenArray;
    }//end if statement
    else {
    //else if (number < list1.length) { //else if statement if random number is within length of array
      int [] shortenArray = new int[list1.length - 1];
      for (int j=0; j<number; j++) { //for statement to iterate through array
        shortenArray[j] = list1[j]; //copy original array into new array
      } //end for statement
      for (int k=number; k<(list1.length-1); k++) { //for statement to iterate through array
        shortenArray[k]=list1[k+1]; //copy original array into new array and skip random number spot
      }//end for statement
      return shortenArray;
    }//end else if statement 
    //return int shorten; //return array to be called
  }//end shorten method
  
  public static void print(int[] list) {
    for (int i=0; i<list.length; i++) {//for loop to iterate through array
      System.out.print(list[i] + ",");
    }//end for loop  
  }//end print method
  
  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    Random randomGenerator = new Random(); //create random number generator
    int[] firstArray = generate(); //generate one array with generate method
    int[] secondArray; //declare second array
    int [] insertArray; //declare insert array
    int [] shortenArray; //declare shorten array
    int randomNumber; //declare random number
    System.out.println("Do you want to run the insert method or the shorten method. Please enter 'insert' or 'shorten'"); 
    String userInput = myScanner.next(); //assign input to user variable
    if (userInput.equals("insert")) { //if user inputs insert
      secondArray = generate(); //generate second array with generate method
      insertArray = insert(firstArray, secondArray); //call insert method and assign to new array
      System.out.print("Input 1: {"); //print first array
      print(firstArray); //print first array
      System.out.print("}"); //print first array
      System.out.println(); //move to next line
      System.out.print("Input 2: {"); //print second array
      print(secondArray); //print second array
      System.out.print("}"); //print second array
      System.out.println(); //move to next line
      System.out.print("Output: {"); //print output array
      print(insertArray); //print output array
      System.out.print("}"); //print output array
      System.out.println(); //move to next line
    }//end if statement for insert
    else if (userInput.equals("shorten")) { //if user inputs shorten
      randomNumber=randomGenerator.nextInt(31); //generate random number between 0 and 30 
      shortenArray=shorten(firstArray, randomNumber);
      System.out.print("Input 1: {"); //print first array
      print(firstArray); //print first array
      System.out.print("}"); //print first array
      System.out.println(); //move to next line
      System.out.println("Input 2: " + randomNumber);
      System.out.print("Output: {"); //print output array
      print(shortenArray); //print output array
      System.out.print("}"); //print output array
      System.out.println(); //move to next line
    }//end else if statement for shorten
  }//end main method  
} //end class