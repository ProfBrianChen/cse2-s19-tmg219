/*
Name: Taylor Gomez
Date: 2/15/19
CSE 002 Card Generator - Use random number generator to choose a number from 1-52 and return number and suit
*/
public class CardGenerator { //class
  // main method 
  public static void main(String[] args) {
    //generate random number
    int randomNumber = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    //System.out.println("randomNumber is "+randomNumber); //debug statement to make sure random number correctly paired to card identity and suit
    //create variables
    String suitName; //declare variable for name of suit 
    String cardIdentity=""; //declare variable for identity of card (number)
    //assign the suit name
    if (randomNumber<=13) { //if statement for numbers less than or equal to 13
      suitName="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber>13 && randomNumber<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber>26 && randomNumber<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName="Spades"; //assign suit name to spades
    } //end else statement
    //assign card identity
    int numberCard=randomNumber % 13; //find remainder in order to differentiate value of face cards from number cards
    switch (numberCard) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard=0
        cardIdentity="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard=1
        cardIdentity="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard=11
        cardIdentity="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard=12
        cardIdentity="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard>=2 && numberCard<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity=Integer.toString(numberCard); //assign integer value to string for card number
    } //end if statement 
    //print output variables
    System.out.println("You picked the " + cardIdentity + " of "+ suitName); //print out name of randomly selected card
  } //end main method
} //end class