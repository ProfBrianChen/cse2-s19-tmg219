/*
Name: Taylor Gomez
Date: 01/27/19 
Due: 01/29/19
Course: CSE 002
Homework Assignment 1: Welcome Class
*/
public class WelcomeClass { //define class
  public static void main (String args[]) { //define main
    //print word "welcome" in box to terminal window
    System.out.println("  -----------     ");
    System.out.println("  | WELCOME |     ");
    System.out.println("  -----------     ");
    //print lehigh network id to terminal window under welcome box
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-T--M--G--2--1--9->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    //print tweet-length autobiographic statement to terminal window under lehigh network id
    System.out.println("My name is Taylor Gomez. I am a senior Bioengineering major from NYC and I have a Morkie named PJ.");
  }//end main
}//end class