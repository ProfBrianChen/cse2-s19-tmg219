/*
Name: Taylor Gomez
Date: 3/19/19
CSE 002 HW 06 Network - ask user for width and height of display window and size of boxes and edge length between boxes to display a window into a “network” of boxes connected by lines
*/
import java.util.Scanner;
public class Network {
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
	  int width; //declare width variable for display window
    int height; //declare height variable for display window
    int boxSize; //declare box size variable for size of each box
    int edge; //declare edge variable for length of connection between boxes
	  String wrongInput; //declare variable to use if input is not an integer
    //ask user for height input and check if user provided integer
    System.out.println("Please provide the desired height of display window");
    while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  height = myScanner.nextInt(); //assign correct input to height variable
      if (height >0){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
    }//end while loop for height input
    //ask user for width input and check if user provided integer
    System.out.println("Please provide the desired width of display window");
    while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  width = myScanner.nextInt(); //assign correct input to width variable
      if (width >0){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
    }//end while loop for width input 
    //ask user for box size input and check if user provided integer
    System.out.println("Please provide the desired size of each box");
    while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  boxSize = myScanner.nextInt(); //assign correct input to box size variable
      if (boxSize >0){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
    }//end while loop for box size input
    //ask user for edge input and check if user provided integer
    System.out.println("Please provide the desired edge length between the boxes");
    while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  edge = myScanner.nextInt(); //assign correct input to width variable
      if (edge >0){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
    }//end while loop for edge input
    int sideType = boxSize%2; //declare variable to determine if square size is even or odd
    int edgePlacement = boxSize/2; //declare variable to determine which row of square the edge should be printed in
    int rowPlace = 0; //declare variable to determine place in row to print 
    int columnPlace = 0; //declare variable to determine place in column to print
    for (int numRows=0; numRows<height; numRows++) { //for loop for number of rows in display window (height)
      for (int numColumns=0; numColumns<width; numColumns++) { //for loop for number of columns of display window (width)
        columnPlace=numColumns%(boxSize+edge); //set value of column place depending on size of square and edge length and value of numColumns at specific point in time
        rowPlace=numRows%(boxSize+edge); //set value of row place depending on size of square and edge length and value of numRows at specific point in time
        if ((rowPlace==0 || rowPlace==boxSize-1) && (columnPlace==0 || columnPlace==boxSize-1)) { //if statement to determine corner of each square
          System.out.print("#"); //print out pound sign for each corner
        }//end if statement to determine corner of each square
        else if ((rowPlace==0 || rowPlace==boxSize-1) && (columnPlace>=1 && columnPlace<=boxSize-2)) { //else if statement for top and bottom edge of each square
          System.out.print("-"); //print out dash for top and bottom edge of each square
        }//end else if statement for top and bottom edge of each square
        else if ((rowPlace>=1 && rowPlace<=boxSize-2) && (columnPlace==0 || columnPlace==boxSize-1)) { //else if statement for sides of each square
          System.out.print("|"); //print out pipe character for sides of each square
        } //end else if statement for sides of each square
        else if ((rowPlace>=1 && rowPlace<=boxSize-2) && (columnPlace>=1 && columnPlace<=boxSize-2)) { //else if statement for middle of each square
          System.out.print(" "); //print out space for middle of each square
        } // end else if statement for middle of each square
        if (sideType==0) { //if statement for if square size is even (changes edge type)
          if ((rowPlace==edgePlacement-1 || rowPlace==edgePlacement) && (columnPlace>=boxSize && columnPlace<=(boxSize+edge-1))) { //if statement for placement of double width edges between two squares horizontally
            System.out.print("-"); //print out dash for double width edges between squares horizontally
          }//end if statement for placement of double width edges between two squares horizontally
          else if ((rowPlace<edgePlacement-1 || rowPlace>edgePlacement) && (columnPlace>=boxSize && columnPlace<=(boxSize+edge-1))) { //else if statement for blank spaces between two squares horizontally
            System.out.print(" "); //print out space between two squares horizontally
          } //end else if statement for blank spaces between two squares horizontally
          else if ((rowPlace>=boxSize && rowPlace<=(boxSize+edge-1)) && (columnPlace==edgePlacement-1 || columnPlace==edgePlacement)) { //else if statement for placement of double width edges between two squares vertically
            System.out.print("|"); //print out pipe character for double width edges between squares vertically 
          } //end else if statement for placement of double width edges between two squares vertically
          else if ((rowPlace>=boxSize && rowPlace<=(boxSize+edge-1) && (columnPlace<edgePlacement-1 || columnPlace>edgePlacement))) { //else if statement for blank spaces between two squares vertically
            System.out.print(" "); //print out space between two squares vertically 
          } //end else if statement for blank spaces between two squares vertically
        } //end if statement for if sqaure size is even (changes edge type)
        if (sideType==1) { //if statement for if square size is odd (changes edge type)
          if ((rowPlace==edgePlacement) && (columnPlace>=boxSize && columnPlace<=(boxSize+edge-1))) { //if statement for placement of single width edge between two squares horizontally
            System.out.print("-"); //print out dash for single width edge between two squares horizontally
          } //end if statement for placement of single width edge between two squares horizontally
          else if ((rowPlace!=edgePlacement) && (columnPlace>=boxSize && columnPlace<=(boxSize+edge-1))) { //if statement for blank spaces between two squares horizontally
            System.out.print(" ");//print out spaces between two squares horizontally
          } //end if statement for blank spaces between two squares horizontally
          else if ((rowPlace>=boxSize && rowPlace<=(boxSize+edge-1)) && (columnPlace==edgePlacement)) { //if statement for single width edge between two squares vertically
            System.out.print("|"); //print out pipe character for single width edge between two squares vertically
          } //end if statement for single width edge between two squares vertically
          else if ((rowPlace>=boxSize && rowPlace<=(boxSize+edge-1)) && (columnPlace!=edgePlacement)) { //if statement for blank spaces between two squares vertically
            System.out.print(" "); //print out space between two squares vertically
          } //if statement for blank spaces between two squares vertically
        } //end if statement for if square size is odd (changes edge type)
      }//end for loop for number of columns of display window (width)
      System.out.println(); //move to new row
    }//end for loop for number of rows of display window (height)
  }//end main method
}//end class