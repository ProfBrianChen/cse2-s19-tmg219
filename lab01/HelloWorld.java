//CSE 002 Hello World
import java.util.Scanner;
import java.util.Arrays; //import arrays
public class HelloWorld {
   public static int findCard (int [] originalHand, int k) {
      int [] hand = new int [originalHand.length]; //create new variable to avoid pass by reference
      for (int z=0; z<hand.length; z++) {
        hand[z]=originalHand[z]; //copy arrays
      }//end for loop 
      if (k>5 || k<0) {
        return -1; //return value
      }//end if loop 
      int min=999; //create minimum variable
      int index=-1; //create variable
      int value; //create variable
      for (int i=0; i<k; i++) {
        for (int j=0; j<hand.length; j++) {
          value=(hand[j]%13);//determine value of cards
          if (min>value) { //if new minimum exists
            min = value; //set variable equal to value of another variable
            index = j; //set variable equal to value of another variable
          }//end if statement
        }//end j loop
        if (i==(k-1)){
          break; //break out of loop 
        }
          hand[index]=999; //set value equal to 999 (high number)
          min=999;
          index=-1; 
      }//end i for loop 
      return index; 
    }//end find card method
  
  public static void main(String args[]) {
    //System.out.println("Hello, World"); //prints the statement "Hello World" into terminal window
    int[] pokerHand = new int[5]; //declare poker hand
    pokerHand[0]=5;
    pokerHand[1]=1;
    pokerHand[2]=4;
    pokerHand[3]=3;
    pokerHand[4]=15;
    int min = findCard(pokerHand, 1);
    int min2 = findCard(pokerHand, 2);
    int min3 = findCard(pokerHand, 3);
    int min4 = findCard(pokerHand, 4);
    int min5 = findCard(pokerHand, 5);
    System.out.println(min + " " + min2 + " " + min3 + " " + min4 + " " + min5);
    
    
  } //ends main
} //ends class