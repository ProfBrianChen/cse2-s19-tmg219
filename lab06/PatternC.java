/*
Name: Taylor Gomez
Date: 3/8/19
CSE 002 Lab 06 PatternC - ask user for integer input between 1 and 10 and print out specified pyramid with input number of rows
*/
import java.util.Scanner;
public class PatternC {
	//main method
	public static void main(String[] args) {
	  Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
	  int length; //declare length variable for # of rows of pyramid
	  String wrongInput; //declare variable to use if input is not an integer
    int counter=1; //declare variable to use in for loops
	  //ask user for integer input and check if user provided integer between 1 and 10
	  System.out.println("Please provide a positive integer value between 1 and 10.");
	  while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  length = myScanner.nextInt(); //assign correct input to length variable 
      if (length<10 && length>1){ //if statement to check if integer input is between 1 and 10 (inclusive)
        break; //leave loop
      } //end if statement
    	System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
      }//end while loop
	  for (int numRows = 1; numRows<length+1; numRows++) { //for loop for number of rows in pyramid
      //int spaces=length-numRows; 
      for (int spaces=length-numRows; spaces>=1; spaces--) {
        System.out.print(" ");
      }
      for (int patternLine=numRows; patternLine>=1; patternLine--) { //for loop for pattern for line
        System.out.print(patternLine); 
      } //end for loop for pattern for line
      System.out.println(); //move to new lines 
    }//end outer for loop for number of rows   
	}//end main method
}//end class