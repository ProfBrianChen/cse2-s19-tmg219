/*
Name: Taylor Gomez
Date: 3/6/19
CSE 002 Homework 5 - ask user for information on a course (course number, department name, number of times it meets per week,
                     time class starts, instructor name, number of students) and check if input is correct type
*/
import java.util.Scanner; //import scanner class 
public class Hw05 { //class
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    //declare variables
    int courseNumber; //declare variable for course number
    String departmentName; //declare variable for department name
    int numberClassTimesWeekly; //declare variable for number of times a class meets per week
    String startTime; //declare variable for start time of class
    String instructorName; //declare variable for name of instructor 
    int numberStudents; //declare variable for number of students in class
    String wrongInput; ////declare variable to use if input is not correct type
    String blankLine; //declare variable to create a new line after integer is read (first time)
    String blankLine2; //declare variable to create a new lines after integer is read (second time)
    //ask user for course number (positive int)
    System.out.print("Enter the course number (needs to be a positive integer): "); //ask user to enter course number
    while (true)  { //run infinite while loop to check input is integer and positive for course number
      while (!myScanner.hasNextInt()) { //while loop if input is not an integer
			  wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.print("You did not enter a correct course number. Please try again with a positive integer: "); //ask user for new input 
		  } //end while loop for if input is not an integer
		  courseNumber = myScanner.nextInt(); //assign correct input to course number variable
      if (courseNumber >0){ //if statement to check if integer input is positive
        break; //leave while loop
      } //end if statement
      System.out.print("You did not enter a correct course number. Please try again with a positive integer: "); //ask user for new input 
    }//end while loop to check if input is integer and positive for course number
    blankLine = myScanner.nextLine(); //next input is read as line (in order for next string to be read)
    
    //ask user for department name (String)
    System.out.print("Enter the name of the department for the course (needs to be a String): "); //ask user to enter department name
    while (!myScanner.hasNextLine()) { //while loop to check if input is a string for department name
      wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
      System.out.print("You did not enter a string for department name. Please try again: "); //ask user for new input
    } //end while loop to check if input is a string for department name
    departmentName = myScanner.nextLine(); //assign correct input to department name variable
    
    //ask user for number of times class meets per week (positive integer)
    System.out.print("Enter the number of times the class meets per week (needs to be a positive integer): "); //ask user to enter times class meets weekly
    while (true)  { //run infinite while loop to check input is integer and positive for number of class times
      while (!myScanner.hasNextInt()) { //while loop if input is not an integer
			  wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.print("You did not enter a positive integer for number of class meeting times per week. Please try again: "); //ask user for new input 
		  } //end while loop for if input is not an integer
		  numberClassTimesWeekly = myScanner.nextInt(); //assign correct input to number of class times variable
      if (numberClassTimesWeekly >0){ //if statement to check if integer input is positive
        break; //leave while loop
      } //end if statement
      System.out.print("You did not enter a positive integer for number of class meeting times per week. Please try again: "); //ask user for new input 
    }//end while loop to check if input is integer and positive for number of times class meets per week
    blankLine2 = myScanner.nextLine(); //next input is read as line (in order for next string to be read)
    
    //ask user for start time of class (string)
    System.out.print("Enter the start time for the course (needs to be a String in the form 00:00 AM or 00:00 PM): "); //ask user to enter class start time
    while (!myScanner.hasNextLine()) { //while loop to check if input is a string for start time
      wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
      System.out.print("You did not enter a string for start time. Please try again (00:00 AM or 00:00 PM): "); //ask user for new input
    } //end while loop to check if input is a string for start time
    startTime = myScanner.nextLine(); //assign correct input to start time variable
    
    //ask user for instructor name (string)
    System.out.print("Enter the name of the instructor for the course (needs to be a String): "); //ask user to enter instructor name
    while (!myScanner.hasNextLine()) { //while loop to check if input is a string for instructor name
      wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
      System.out.print("You did not enter a string for instructor name. Please try again: "); //ask user for new input
    } //end while loop to check if input is a string for instructor name
    instructorName = myScanner.nextLine(); //assign correct input to instructor name variable
    
    //ask user for number of students in class
    System.out.print("Enter the number of students in the class (needs to be a postive integer): "); //ask user to input number of students in class
    while (true)  { //run infinite while loop to check input is integer and positive for number of students in class
      while (!myScanner.hasNextInt()) { //while loop if input is not an integer
			  wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.print("You did not enter a positive integer for number of students in the class. Please try again: "); //ask user for new input 
		  } //end while loop for if input is not an integer
		  numberStudents = myScanner.nextInt(); //assign correct input to number of students variable
      if (numberStudents >0){ //if statement to check if integer input is positive
        break; //leave while loop
      } //end if statement
      System.out.print("You did not enter a positive integer for number of students in the class. Please try again: "); //ask user for new input 
    }//end while loop to check if input is integer and positive for number of students
    
    //print input variables from user
    System.out.println("The course number is " + courseNumber); //print out course number
    System.out.println("The department name is " + departmentName); //print out department name
    System.out.println("The class meets " + numberClassTimesWeekly + " time(s) a week."); //print out number of times class meets per week
    System.out.println("The class starts at " + startTime); //print out start time of class
    System.out.println("The instructor's name is " + instructorName); //print out name of instructor 
    System.out.println("There are " + numberStudents + " in the class"); //print out number of students in class
  }//end main method
}//end class 
