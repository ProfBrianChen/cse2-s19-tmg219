/*
Name: Taylor Gomez
Date: 4/12/19
CSE 002 lab 09 - Ask user for linear or binary search choice and create multiple methods depending on user choice
*/
import java.util.Arrays; //import arrays
import java.util.Random; //import random generator 
import java.util.Scanner; //import scanner 
public class Searching {
  public static int[] linearGenerate(int arraySize) {
    Random randomGenerator = new Random(); //create random number generator
    int [] linearArray = new int[arraySize]; //declare array of random length
    int counter=1; //declare counter
    linearArray[0]=randomGenerator.nextInt(arraySize+1); //set first space in array to randomly generated number between 0 and size of array 
    for (int i=1; i<linearArray.length; i++) { //for loop to iterate through array
      linearArray[i]=randomGenerator.nextInt(arraySize+1); //at ith position, assign random generated number
      counter++; //add one to counter
      for (int j=0; j<counter; j++) { //for loop to iterate through array
        if ((linearArray[i]==linearArray[j]) && (i!=j)) { //if statement to check for duplicates
          linearArray[i]=randomGenerator.nextInt(arraySize+1); //at ith position, assign random generated number
        } //end if statement
      } //end j for loop  
    } //end i for loop
    return linearArray; //return array to be called 
  }//end linear generate method
  
  public static int[] binaryGenerate(int arraySize) {
    Random randomGenerator = new Random(); //create random number generator
    int [] binaryArray = new int[arraySize]; //declare array of random length
    binaryArray[0]=randomGenerator.nextInt(arraySize+1); //set first space in array to randomly generated number between 0 and size of array 
    for (int i=1; i<binaryArray.length; i++) { //for loop to iterate through array
      binaryArray[i]= (binaryArray[i-1] + randomGenerator.nextInt(10)+1); //at ith position, assign random generated number and add to previous spot in array for ascending order
    } //end i for loop
    return binaryArray; //return array to be called 
  }//end binary generate method
  
  public static int linearSearch(int[] array, int searchTerm) {
    int number=-1; //declare and set variable equal to -1
    for (int i=0; i<array.length; i++) {//for loop to iterate through array 
      if (array[i]!=searchTerm){
        continue; //continue through loop
      } //end if statement
      else { 
        number = i; //set variable equal to i 
        break; //break out of loop
      }//end else statement
    }//end for loop
    return number; //return variable to be called
  }//end linear search method
  
  public static int binarySearch(int[] array, int searchTerm) {
    int searchIndex = Arrays.binarySearch(array, searchTerm); //find index of number
    int number = -1; //declare and set variable equal to -1
    if (searchIndex>=0) {
      number = searchIndex; //set number equal to the searchIndex variable
    } //end if statement
    return number; //return variable to be called
  }//end binary search method
  
  public static void main (String[] args) { //main method
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    int userArraySize; //declare variable for user's array size
    int userSearch; //declare variable for user's search term
    int[] linearArray; //declare linear array
    int[] binaryArray; //declare binary array
    int indexArray; //declare variable for method output
    System.out.println("Do you want to perform a linear or binary search. Please input 'linear' or 'binary'"); //ask user for choice
    String userInput = myScanner.next(); //assign input to user variable
    if (userInput.equals("linear")) {
      System.out.println("What is the size of the array. Please input a positive integer"); //ask user for choice
      userArraySize = myScanner.nextInt(); //assign input to user variable
      System.out.println("What is the integer search term? Please input a positive integer"); //ask user for choice
      userSearch=myScanner.nextInt(); //assign input to user variable
      linearArray = linearGenerate(userArraySize); //generate one array with linear generate method
      indexArray = linearSearch(linearArray,userSearch); //call array to search for user's search term
      System.out.print("The array is: [ "); //print out array
      for (int i=0; i<linearArray.length; i++) { //for loop to iterate through array
        System.out.print(linearArray[i] + " "); //print array
      }//end for loop 
      System.out.println("]"); //move to next linearArray
      System.out.println("The search term you chose can be found at: " + indexArray); //print out where in array search term was found
    }//end if statement for linear
    if (userInput.equals("binary")) {
      System.out.println("What is the size of the array. Please input a positive integer"); //ask user for choice
      userArraySize = myScanner.nextInt(); //assign input to user variable
      System.out.println("What is the integer search term? Please input a positive integer"); //ask user for choice
      userSearch=myScanner.nextInt(); //assign input to user variable
      binaryArray = binaryGenerate(userArraySize); //generate one array with linear generate method
      indexArray = binarySearch(binaryArray,userSearch); //call array to search for user's search term
      System.out.print("The array is: ["); //print out array
      for (int i=0; i<binaryArray.length; i++) { //for loop to iterate through array
        System.out.print(binaryArray[i] + " "); //print array
      }//end for loop 
      System.out.println(); //move to next linearArray
      System.out.println("The search term you chose can be found at: " + indexArray); //print out where in array search term was found
    }//end if statement for binary 
  }//end main method
} //end class