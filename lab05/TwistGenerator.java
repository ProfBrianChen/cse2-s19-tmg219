/*
Name: Taylor Gomez
Date: 3/1/19
CSE 002 Twist Generator - ask user for integer input and print out twist based on input, which corresponds to length of twist
*/
import java.util.Scanner; //import scanner class 
public class TwistGenerator { //class
	//main method
	public static void main(String[] args) { 
		Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
		int length; //declare length variable
		String junkWord; //declare variable to use if input is not an integer
		//ask user for integer input and check if user provided integer
		System.out.println("Please provide a positive integer value.");
		while (true)  { //run while loop to check input is integer and positive
      while (!myScanner.hasNextInt()) { //if not an integer, this loop will run 
			  junkWord = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.println("You did not enter an integer. Please try again."); //ask user for new input 
		  } //end while loop
		  length = myScanner.nextInt(); //assign correct input to length variable
      if (length >0){ //if statement to check if integer input is positive
        break; //leave loop
      } //end if statement
      System.out.println("You did not enter a positive integer. Please try again."); //ask user for new input 
    }//end while loop
    for (int i=0; i<3; i++) { //for loop for number of lines in normal twist (3)
      for (int j=0; j<length; j++) { //for loop for characters to include in each line
        if (i==0) { //if loop for first line of twist
          if (j%3==0) { //if loop for first part of pattern in twist
            System.out.print("\\"); //print out first part of pattern in twist
          }//end j%3=0 if loop
          if (j%3==1) { //if loop for second part of pattern in twist
            System.out.print(" "); //print out second part of pattern in twist
          }//end j%3=1 if loop
          if (j%3==2) { //if loop for third part of pattern in twist
            System.out.print("/"); //print out third part of pattern in twist
          }//end j%3=2 if loop
        }//end i=0 if loop
        if (i==1) { //if loop for second line of twist
          if (j%3==0) { //if loop for first part of pattern in twist
            System.out.print(" "); //print out first part of pattern in twist
          }//end j%3=0 if loop
          if (j%3==1) { //if loop for second part of pattern in twist
            System.out.print("X"); //print out second part of pattern in twist
          }//end j%3=1 if loop
          if (j%3==2) { //if loop for third part of pattern in twist
            System.out.print(" "); //print out third part of pattern in twist
          }//end j%3=2 if loop
        }//end i=1 if loop
        if (i==2) { //if loop for first line of twist
          if (j%3==0) { //if loop for first part of pattern in twist
            System.out.print("/"); //print out first part of pattern in twist
          }//end j%3=0 if loop
          if (j%3==1) { //if loop for second part of pattern in twist
            System.out.print(" "); //print out second part of pattern in twist
          }//end j%3=1 if loop
          if (j%3==2) { //if loop for third part of pattern in twist
            System.out.print("\\"); //print out third part of pattern in twist
          }//end j%3=2 if loop
        }//end i=2 if loop
      } //end for loop for length of each line
      System.out.println(); //move to new line 
    } //end for loop for number of lines in twist
	} //end main method
} //end class