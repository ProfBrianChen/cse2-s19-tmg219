/*
Name: Taylor Gomez
Date: 3/26/19
CSE 002 HW 07 Area - Calculate area of three shapes based on user's input of width and height and choice of shape
*/
import java.util.Scanner; //import scanner 
public class Area {
  public static double rectArea( double width, double height ) { //method for rectangle area
    double areaR = width*height; //calculate rectangle area with inputs
    return areaR; //return rectangle area
  }//end method for rectangle area
  public static double triArea ( double base, double height) { //method for triangle area
    double areaT = (base*height)/2; //calculate triangle area with inputs
    return areaT; //return triangle area
  }//end method for triangle area
  public static double circArea ( double radius ) { //method for circle area
    double areaC = Math.PI * Math.pow(radius, 2); //calculate circle area with inputs
    return areaC; //return circle area
  }//end method for circle area
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    String rectangle = "rectangle"; //declare variable to use to compare string for input
    String triangle = "triangle"; //declare variable to use to compare string for input
    String circle = "circle"; //declare variable to use to compare string for input
    double width; //declare width variable for rectangle
    double height; //declare height variable for rectangle
    double base; //declare base variable for triangle
    double heightTri; //declare height variable for triangle
    double radius; //declare radius variable for circle
    String wrongInput=""; //declare wrong input variable 
    System.out.println("Please choose one of the following shapes: rectangle, triangle or circle"); //ask user to choose one of three given shapes
    String shape = myScanner.next(); //assign correct input to shape variable
    while (true)  { //run while loop to check input is one of acceptable strings
      while (!shape.equals(rectangle) && !shape.equals(triangle) && !shape.equals(circle)) { //if not one of given shapes, this loop will run 
			  System.out.println("You did not enter one of chosen shapes. Please try again and choose either rectangle, triangle or circle. (No caps)"); //ask user for new input 
        shape=myScanner.next(); //assign correct input to shape variable
		  } //end while loop
      break; //leave loop
    }//end while loop for acceptable string input
    if (shape.equals(rectangle)) { //if statement for if user chose rectangle
      System.out.println("What is the height of the rectangle?"); //ask user for height of rectangle
      while (true)  { //run while loop to check input is double and positive
        while (!myScanner.hasNextDouble()) { //if not an double, this loop will run 
			    wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			    System.out.println("You did not enter a double for the rectangle height. Please try again."); //ask user for new input 
		    } //end while loop
		    height = myScanner.nextDouble(); //assign correct input to height variable
        if (height >0){ //if statement to check if double input is positive
          break; //leave loop
        } //end if statement
        System.out.println("You did not enter a positive double for the rectangle height. Please try again."); //ask user for new input 
       }//end while loop for height input
      System.out.println("What is the width of the rectangle?"); //ask user for width of rectangle
      while (true)  { //run while loop to check input is double and positive
        while (!myScanner.hasNextDouble()) { //if not an double, this loop will run 
			    wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			    System.out.println("You did not enter an double for the rectangle width. Please try again."); //ask user for new input 
		    } //end while loop
		    width = myScanner.nextDouble(); //assign correct input to height variable
        if (width >0){ //if statement to check if double input is positive
          break; //leave loop
        } //end if statement
        System.out.println("You did not enter a positive double for the rectangle width. Please try again."); //ask user for new input 
      }//end while loop for width input
      double areaRect = rectArea(width, height); //call rectangle area method
      System.out.println("The area of the rectangle is: " + areaRect); //print area of rectangle 
    }//end if statement for rectangle
    if (shape.equals(triangle)) { //if statement for triangle
      System.out.println("What is the base of the triangle?"); //ask user for base of triangle
      while (true)  { //run while loop to check input is double and positive
        while (!myScanner.hasNextDouble()) { //if not an double, this loop will run 
			    wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			    System.out.println("You did not enter a double for the triangle base. Please try again."); //ask user for new input 
		    } //end while loop
		    base = myScanner.nextDouble(); //assign correct input to base variable
        if (base >0){ //if statement to check if double input is positive
          break; //leave loop
        } //end if statement
        System.out.println("You did not enter a positive double for the triangle base. Please try again."); //ask user for new input 
       }//end while loop for base input
      System.out.println("What is the height of the triangle?"); //ask user for height of triangle
      while (true)  { //run while loop to check input is double and positive
        while (!myScanner.hasNextDouble()) { //if not an double, this loop will run 
			    wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			    System.out.println("You did not enter an double for the triangle height. Please try again."); //ask user for new input 
		    } //end while loop
		    heightTri = myScanner.nextDouble(); //assign correct input to height variable
        if (heightTri >0){ //if statement to check if double input is positive
          break; //leave loop
        } //end if statement
        System.out.println("You did not enter a positive double for the rectangle height. Please try again."); //ask user for new input 
       }//end while loop for height input
      double areaTri = triArea(base, heightTri); //call method for triangle area
      System.out.println("The area for the triangle is: " + areaTri); //print area of triangle
    }//end if statement for triangle
    if (shape.equals(circle)) { //if statement for circle
      System.out.println("What is the radius of the circle?"); //ask user for input for radius of circle
      while (true)  { //run while loop to check input is double and positive
        while (!myScanner.hasNextDouble()) { //if not an double, this loop will run 
			    wrongInput = myScanner.next(); //assign value to wrong input variable to delete user input in order to try again
			    System.out.println("You did not enter a double for the circle radius. Please try again."); //ask user for new input 
		    } //end while loop
		    radius = myScanner.nextDouble(); //assign correct input to height variable
        if (radius >0){ //if statement to check if double input is positive
          break; //leave loop
        } //end if statement
        System.out.println("You did not enter a positive double for the circle radius. Please try again."); //ask user for new input 
       }//end while loop for radius input
      double areaCirc = circArea(radius); //call method for circle area
      System.out.println("The area for the circle is: " + areaCirc); //print out circle area
    }//end if statement for circle
  }//end main method
}//end class