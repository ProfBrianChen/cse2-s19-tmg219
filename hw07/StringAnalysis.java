/*
Name: Taylor Gomez
Date: 3/26/19
CSE 002 HW 07 String Analysis - Process a string by examining all the characters or a specific number of characters in the string (based on user input) 
and determine if they are letters. 
*/
import java.util.Scanner; //import scanner 
public class StringAnalysis {
  public static boolean checkCharacters (String characterInput) { //checkCharacters method that only accepts string and returns a boolean
    boolean check=true; //declare boolean value to return 
    boolean letterCheck; //declare boolean to check if character is a letter
    int counter; //declare counter variable
    int counter1=0; //declare counter variable 
    int stringLength=characterInput.length(); //declare length variable of length of string
    char letter; //declare char variable 
    while ((check==true)&&(counter1<stringLength)) { //while loop to continue checking each letter of string if conditions are met
      for (counter=0; counter<stringLength; counter++) { //for loop that increases counter to move over to each position in string
        letter=characterInput.charAt(counter); //as counter increases, check letter at each position along string
        letterCheck=Character.isLetter(letter); //determines if character is a letter and sets value to true or false
        if (letterCheck==true){ //if statement if character is letter
          check=true; //set value of check equal to true
        }//end if statement if character is letter
        else { //else statement if character is not a letter
          check=false; //set value of check equal to false
        } //end else statement if character is not a letter
        counter1++; //increase counter by 1
      } //end for loop that increases counter to move over to each position in string
    } //end while loop to check each letter of string
    return check; //return variable to be used when method is called
  }//end checkCharacters method (only input is string)
  public static boolean checkCharacters (String characterInput, int checkNumber) { //checkCharacters method that accepts string and int and returns a boolean
    boolean check=true; //declare boolean value to return 
    boolean letterCheck=true; //declare boolean to check if character is a letter
    int counter; //declare counter variable
    int counter1=0; //declare counter variable 
    int stringLength=characterInput.length(); //declare length variable of length of string
    char letter; //declare char variable
    //determine if user int input is greater than or less than length of string 
    if (checkNumber<stringLength) { //if statement if user input is less than the length of the string
      checkNumber=checkNumber; //set checkNumber variable equal to itself so it only checks that number of characters
    } //end if statement if user input is less than the length of the string
    else {//else statement if user input is greater than the length of the string
      checkNumber=stringLength; //set checkNumber variable equal to the length of the string (max possible number of characters it can check)
    } //end else statement if user input is greater than the length of the string
    while ((check==true)&&(counter1<stringLength)) { //while loop to continue checking each letter of string if conditions are met
      for (counter=0; counter<checkNumber; counter++) { //for loop that increases counter to move over to each position in string
        letter=characterInput.charAt(counter);; //as counter increases, check letter at each position along string
        letterCheck=Character.isLetter(letter); //determines if character is a letter and sets value to true or false
        if (letterCheck==true){ //if statement if character is letter
          check=true; //set value of check equal to true
        }//end if statement if character is letter
        else { //else statement if character is not a letter
          check=false; //set value of check equal to false
        } //end else statement if character is not a letter
        counter1++; //increase check by 1
      } //end for loop that increases counter to move over to each position in string
    } //end while loop to check each letter of string
    return check; //return variable to be used when method is called
  }//end checkCharacters method (input is string and int)
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    boolean check=false; //declare boolean value 
    int examinePart; //declare variable for if user wants to examine a specific number of characters in string
    String wrongInput=""; //declare variable to use if user input is not correct
    System.out.println("What string would you like to examine for letters?"); //ask user for input of string
    String userStringInput=myScanner.next(); //assign user input to variable
    System.out.println("Would you like to examine the entire string? Please enter yes or no."); //ask user if they want to examine entire string
    String examineWhole = myScanner.next(); //assign user input to variable
    while (!examineWhole.equalsIgnoreCase("yes") && !examineWhole.equalsIgnoreCase("no")) { //while loop if user does not enter yes or no
      System.out.println("That was not an appropriate answer. Please try again with yes or no"); //ask user again for yes or no
      examineWhole = myScanner.next(); //assign user input to variable
    } //end while loop if user does not enter yes or no
    if (examineWhole.equals("yes")) { //if statement if user inputs that they want to examine whole string
      check=checkCharacters(userStringInput); //call checkCharacters method that only accpets a string and assign true or false value to boolean 
    }//end if statement if user inputs that they want to examine whole string
    else if (examineWhole.equalsIgnoreCase("no")) { //else if statement if user inputs that they don't want to examine the whole string
      System.out.println("How many characters in the string would you like to check? Please enter a positive integer value"); //ask user for number of characters to check
      while (true)  { //run infinite while loop to check input is integer and positive for number of characters to check
      while (!myScanner.hasNextInt()) { //while loop if input is not an integer
			  wrongInput = myScanner.next(); //assign value to junk variable to delete user input in order to try again
			  System.out.print("You did not enter an integer for how many characters you want to check. Please try again with a positive integer"); //ask user for new input 
		  } //end while loop for if input is not an integer
		  examinePart = myScanner.nextInt(); //assign correct input to examinePart variable
      if (examinePart >0){ //if statement to check if integer input is positive
        break; //leave while loop
      } //end if statement
      System.out.print("You did not enter a positive integer for how many characters you want to check . Please try again with a positive integer"); //ask user for new input 
      }//end while loop to check if input is integer and positive for number of characters to check
      check=checkCharacters(userStringInput, examinePart); //calls checkCharacters method that accepts a string and int and assign a true or false value to boolean
    } //end else if statement if user inputs that they don't want to examine the whole string
    if (check==false) { //if statement for if boolean value is false (not all characters in string are letters)
      System.out.println("In the string you entered, not all of the characters are letters"); //print out that not all of characters in string are letters
    } //end if statement for if boolean value is false (not all characters in string are letters
    else /*if (check==true)*/ {//if statement for if boolean value is true (all characters in string are letters)
      System.out.println("In the string you entered, all of the characters are letters"); //print out that all of characters in string are letters
    }//end else if statement for if boolean value is true (all characters in string are letters)
  } //end main method
}//end class