/*
Name: Taylor Gomez
Date: 3/22/19
CSE 002 Lab 08 Arrays - Create an array of random size, populate with random ints and determine range, mean, std deviation
*/
import java.util.Arrays;
import java.util.Random;
public class Arraylab {
  public static int getRange(int[] list) { //method to find the range of array
    Arrays.sort(list); //sort array from min to max
    int a=list.length-1; //find last spot of array and assign to variable
    int range=list[a]-list[0]; //subtract last value of array (max) by first value of array (min) to find range
    shuffle(list); //call shuffle
    return range; //return range value
  }//end range method
  public static double getMean(int[] list) {//method to find the mean of array
    int n = list.length; //determine size of array and assign to variable
    double sum=0; //declare variable to be used in for loop for sum of values in array
    for (int i=0; i<list.length; i++) { //for loop to determine sum of values in array
      sum += list[i]; //add each value of ith spot to sum variable
    }//end for loop
    double mean=sum/n; //divide sum of values in array by size of array to determine the mean
    return mean; //return mean value 
  }//end mean method
  public static double getStdDev(int[] list, double mean) { //method to find the standard deviation of array
    double sum = 0;
    int denominator=list.length - 1; //determine the value of n-1 (one less than the number of entries in the array) and assign to variable
    for (int i=0; i<list.length; i++) {//for loop to determine value of sum of array[i]-mean
      sum+=Math.pow((list[i]-mean),2); //add each value of (array[i]-mean) to sum variable
    }//end for loop
    double b = sum/denominator; //determine value of calculations inside square root
    double stdDev = Math.pow(b,0.5); //determine value of standard deviation
    return stdDev; //return value of standard deviation
  }//end standard deviation method
  public static void shuffle (int[] list) {//method to shuffle array
    for (int i =0; i<list.length; i++) { //for loop to shuffle array and iterate through each spot
      int randomSpot = (int) (list.length * Math.random() );; //choose random spot in array
      int tempSpot = list[randomSpot]; //assign value of random spot to temporary variable
      list[randomSpot]=list[i]; //assign value of ith spot in array to random spot in array
      list[i]=tempSpot; //assign value of random spot (from temporary variable) to ith spot in array
    }//end for loop
    System.out.println("The shuffled array values are "); //print out line 
    for (int i=0; i<list.length; i++) { //for loop to iterate through array
      System.out.println(list[i]); //print out value of ith spot in array
    }//end for loop
  }//end shuffle method
  public static void main(String[] args) { //main method
    int ranData; 
    Random randomGenerator = new Random(); //create random number generator
    int randomInt = randomGenerator.nextInt(51)+50; //generate random number between 50 and 100 (inclusive) and assign to variable
    int [] randomArray = new int[randomInt]; //declare and allocate array with number of spaces available equal to that of the random number generated
    System.out.println("The size of the array is: " + randomInt); //print size of array
    for (int i =0; i<randomInt; i++) { //for loop to initialize data into array
      ranData=randomGenerator.nextInt(100); //generate a random number between 0 and 99 
      randomArray[i]=ranData; //assign random number generated to ith spot in array
      System.out.println(randomArray[i]); //print out each entry of array 
    }//end for loop
    int valRange=getRange(randomArray); //pass array to getRange and return the range of the array’s values 
    System.out.println("The range is: " + valRange); //print out value of range
    double valMean=getMean(randomArray); //pass array to getMean and return the mean of the array’s values 
    System.out.println("The mean is: " + valMean); //print out value of mean
    double valStdDev=getStdDev(randomArray, valMean); //pass array and mean to getStdDev and return standard deviation of a sample. 
    System.out.println("The standard deviation is: " + valStdDev); //print out value of standard deviation
  }//end main method
}//end class 