/*
Name: Taylor Gomez
Date: 2/19/19
CSE 002 Poker Hand Check - Use random number generator to choose a number from 1-52 (5 times to mimic one card from each deck) 
                          and return whether poker hand is a pair, two pairs, three of a kind or high card hand
*/
public class PokerHandCheck {//class
  //main method
  public static void main(String[] args) {
    //declare variables
    String suitName1; //declare variable for name of suit for card 1
    String cardIdentity1=""; //declare variable for identity of card 1 (number)
    String suitName2; //declare variable for name of suit for card 2
    String cardIdentity2=""; //declare variable for identity of card 2 (number)
    String suitName3; //declare variable for name of suit for card 3
    String cardIdentity3=""; //declare variable for identity of card 3 (number)
    String suitName4; //declare variable for name of suit for card 4
    String cardIdentity4=""; //declare variable for identity of card 4 (number)
    String suitName5; //declare variable for name of suit for card 5
    String cardIdentity5=""; //declare variable for identity of card 5 (number)
    int randomNumber1 = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    int randomNumber2 = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    int randomNumber3 = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    int randomNumber4 = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    int randomNumber5 = (int)(Math.random()*52)+1; //generate random number between 1 and 52 (inclusive) and assign to variable
    //assign random numbers to specific cards (identity and suit)
    //assign the suit name for card 1
    if (randomNumber1<=13) { //if statement for numbers less than or equal to 13
      suitName1="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber1>13 && randomNumber1<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName1="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber1>26 && randomNumber1<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName1="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName1="Spades"; //assign suit name to spades
    } //end else statement 
    
    //assign the suit name for card 2
    if (randomNumber2<=13) { //if statement for numbers less than or equal to 13
      suitName2="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber2>13 && randomNumber2<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName2="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber2>26 && randomNumber2<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName2="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName2="Spades"; //assign suit name to spades
    } //end else statement 
    
    //assign the suit name for card 3
    if (randomNumber3<=13) { //if statement for numbers less than or equal to 13
      suitName3="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber3>13 && randomNumber3<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName3="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber3>26 && randomNumber3<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName3="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName3="Spades"; //assign suit name to spades
    } //end else statement 
    
    //assign the suit name for card 4
    if (randomNumber4<=13) { //if statement for numbers less than or equal to 13
      suitName4="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber4>13 && randomNumber4<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName4="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber4>26 && randomNumber4<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName4="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName4="Spades"; //assign suit name to spades
    } //end else statement 
    
    //assign the suit name for card 5
    if (randomNumber5<=13) { //if statement for numbers less than or equal to 13
      suitName5="Diamonds"; //assign suit name to diamonds
    } //end <= 13 if statement
    else if (randomNumber5>13 && randomNumber5<=26) { //if statement for numbers between 14 and 26 inclusive
      suitName5="Clubs"; //assign suit name to clubs
    } //end 14-26 if statement 
    else if (randomNumber5>26 && randomNumber5<=39) { //if statement for numbers between 26 and 39 inclusive
      suitName5="Hearts"; //assign suit name to clubs
    } //end 26-39 if statement
    else { //else statement for all other numbers not mentioned in if statement
      suitName5="Spades"; //assign suit name to spades
    } //end else statement 
    //determine identity of cards - declare variables
    int numberCard1=randomNumber1 % 13; //find remainder in order to differentiate value of face cards from number cards
    int numberCard2=randomNumber2 % 13; //find remainder in order to differentiate value of face cards from number cards
    int numberCard3=randomNumber3 % 13; //find remainder in order to differentiate value of face cards from number cards
    int numberCard4=randomNumber4 % 13; //find remainder in order to differentiate value of face cards from number cards
    int numberCard5=randomNumber5 % 13; //find remainder in order to differentiate value of face cards from number cards
    //determine identity of card 1
    switch (numberCard1) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard1=0
        cardIdentity1="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard1=1
        cardIdentity1="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard1=11
        cardIdentity1="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard1=12
        cardIdentity1="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard1>=2 && numberCard1<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity1=Integer.toString(numberCard1); //assign integer value to string for card number
    } //end if statement 
    //determine identity of card 2
    switch (numberCard2) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard2=0
        cardIdentity2="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard2=1
        cardIdentity2="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard2=11
        cardIdentity2="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard2=12
        cardIdentity2="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard2>=2 && numberCard2<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity2=Integer.toString(numberCard2); //assign integer value to string for card number
    } //end if statement
    //determine identity of card 3
    switch (numberCard3) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard3=0
        cardIdentity3="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard3=1
        cardIdentity3="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard3=11
        cardIdentity3="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard3=12
        cardIdentity3="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard3>=2 && numberCard3<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity3=Integer.toString(numberCard3); //assign integer value to string for card number
    } //end if statement
    //determine identity of card 4
    switch (numberCard4) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard4=0
        cardIdentity4="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard4=1
        cardIdentity4="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard4=11
        cardIdentity4="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard4=12
        cardIdentity4="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard4>=2 && numberCard4<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity4=Integer.toString(numberCard4); //assign integer value to string for card number
    } //end if statement
    //determine identity of card 5
    switch (numberCard5) { //switch statement to change certain number cards to face cards 
      case 0: //when numberCard5=0
        cardIdentity5="King"; //assign card identity to king
        break; //break statement for case 0
      case 1: //when numberCard5=1
        cardIdentity5="Ace"; //assign card identity to ace
        break; //break statement for case 1
      case 11: //when numberCard5=11
        cardIdentity5="Jack"; //assign card identity to Jack
        break; //break statement for case 11
      case 12: //when numberCard5=12
        cardIdentity5="Queen"; //assign card identity to queen
        break; //break statement for case 12
    } //end switch statement
    if (numberCard5>=2 && numberCard5<=10) { //if statement for when numberCard is between 2 and 10 inclusive
      cardIdentity5=Integer.toString(numberCard5); //assign integer value to string for card number
    } //end if statement
    //print out poker hand 
    System.out.println("Your random cards were:"); //print statement 
    System.out.println("The " + cardIdentity1 + " of "+ suitName1); //print out name of randomly selected card 1 
    System.out.println("The " + cardIdentity2 + " of "+ suitName2); //print out name of randomly selected card 2 
    System.out.println("The " + cardIdentity3 + " of "+ suitName3); //print out name of randomly selected card 3
    System.out.println("The " + cardIdentity4 + " of "+ suitName4); //print out name of randomly selected card 4 
    System.out.println("The " + cardIdentity5 + " of "+ suitName5); //print out name of randomly selected card 5
    //determine type of poker hand and print out type of hand
    //declare counter variables for each card identity
    int aceCounter=0; //declare ace counter
    int twoCounter=0; //decare twos counter
    int threeCounter=0; //declare threes counter
    int fourCounter=0; //declare fours counter
    int fiveCounter=0; //declare fives counter
    int sixCounter=0; //declare sixes counter
    int sevenCounter=0; //declare sevens counter
    int eightCounter=0; //declare eights counter
    int nineCounter=0; //declare nines counter
    int tenCounter=0; //declare tens counter
    int jackCounter=0; //declare jacks counter
    int queenCounter=0; //declare queens counter
    int kingCounter=0; //declare kings counter
    //add values to card identity counters based on identity of card randomly chosen
    switch (numberCard1) { //switch statement for card 1 to add to type of card counter
      case 0: //when numberCard1=0
        kingCounter++; //add one to king counter
        break; //break statement for case 0
      case 1: //when numberCard1=1
        aceCounter++; //add one to ace counter
        break; //break statement for case  1
      case 2: //when numberCard1=2
        twoCounter++; //add one to twos counter
        break; //break statement for case 2
      case 3: //when numberCard1=3
        threeCounter++; //add one to threes counter
        break; //break statement for case 3
      case 4: //when numberCard1=4
        fourCounter++; //add one to fours counter
        break; //break statement for case 4
      case 5: //when numberCard1=5
        fiveCounter++; //add one to fives counter
        break; //break statement for case 5
      case 6: //when numberCard1=6
        sixCounter++; //add one to sixs counter
        break; //break statement for case 6
      case 7: //when numberCard1=7
        sevenCounter++; //add one to sevens counter
        break; //break statement for case 7
      case 8: //when numberCard1=8
        eightCounter++; //add one to eights counter
        break; //break statement for case 8
      case 9: //when numberCard1=9
        nineCounter++; //add one to nines counter
        break; //break statement for case 9
      case 10: //when numberCard1=10
        tenCounter++; //add one to tens counter
        break; //break statement for case 10
      case 11: //when numberCard1=11
        jackCounter++; //add one to jack counter
        break; //break statement for case 11
      case 12: //when numberCard1=12
        queenCounter++; //add one to queen counter
        break; //break statement for case 12
    } //end switch statement for card 1
    switch (numberCard2) { //switch statement for card 2 to add to type of card counter
      case 0: //when numberCard2=0
        kingCounter++; //add one to king counter
        break; //break statement for case 0
      case 1: //when numberCard2=1
        aceCounter++; //add one to ace counter
        break; //break statement for case  1
      case 2: //when numberCard2=2
        twoCounter++; //add one to twos counter
        break; //break statement for case 2
      case 3: //when numberCard2=3
        threeCounter++; //add one to threes counter
        break; //break statement for case 3
      case 4: //when numberCard2=4
        fourCounter++; //add one to fours counter
        break; //break statement for case 4
      case 5: //when numberCard2=5
        fiveCounter++; //add one to fives counter
        break; //break statement for case 5
      case 6: //when numberCard2=6
        sixCounter++; //add one to sixs counter
        break; //break statement for case 6
      case 7: //when numberCard2=7
        sevenCounter++; //add one to sevens counter
        break; //break statement for case 7
      case 8: //when numberCard2=8
        eightCounter++; //add one to eights counter
        break; //break statement for case 8
      case 9: //when numberCard2=9
        nineCounter++; //add one to nines counter
        break; //break statement for case 9
      case 10: //when numberCard2=10
        tenCounter++; //add one to tens counter
        break; //break statement for case 10
      case 11: //when numberCard2=11
        jackCounter++; //add one to jack counter
        break; //break statement for case 11
      case 12: //when numberCard2=12
        queenCounter++; //add one to queen counter
        break; //break statement for case 12
    } //end switch statement for card 2
    switch (numberCard3) { //switch statement for card 3 to add to type of card counter
      case 0: //when numberCard3=0
        kingCounter++; //add one to king counter
        break; //break statement for case 0
      case 1: //when numberCard3=1
        aceCounter++; //add one to ace counter
        break; //break statement for case  1
      case 2: //when numberCard3=2
        twoCounter++; //add one to twos counter
        break; //break statement for case 2
      case 3: //when numberCard3=3
        threeCounter++; //add one to threes counter
        break; //break statement for case 3
      case 4: //when numberCard3=4
        fourCounter++; //add one to fours counter
        break; //break statement for case 4
      case 5: //when numberCard3=5
        fiveCounter++; //add one to fives counter
        break; //break statement for case 5
      case 6: //when numberCard3=6
        sixCounter++; //add one to sixs counter
        break; //break statement for case 6
      case 7: //when numberCard3=7
        sevenCounter++; //add one to sevens counter
        break; //break statement for case 7
      case 8: //when numberCard3=8
        eightCounter++; //add one to eights counter
        break; //break statement for case 8
      case 9: //when numberCard3=9
        nineCounter++; //add one to nines counter
        break; //break statement for case 9
      case 10: //when numberCard3=10
        tenCounter++; //add one to tens counter
        break; //break statement for case 10
      case 11: //when numberCard3=11
        jackCounter++; //add one to jack counter
        break; //break statement for case 11
      case 12: //when numberCard3=12
        queenCounter++; //add one to queen counter
        break; //break statement for case 12
    } //end switch statement for card 3
    switch (numberCard4) { //switch statement for card 4 to add to type of card counter
      case 0: //when numberCard4=0
        kingCounter++; //add one to king counter
        break; //break statement for case 0
      case 1: //when numberCard4=1
        aceCounter++; //add one to ace counter
        break; //break statement for case  1
      case 2: //when numberCard4=2
        twoCounter++; //add one to twos counter
        break; //break statement for case 2
      case 3: //when numberCard4=3
        threeCounter++; //add one to threes counter
        break; //break statement for case 3
      case 4: //when numberCard4=4
        fourCounter++; //add one to fours counter
        break; //break statement for case 4
      case 5: //when numberCard4=5
        fiveCounter++; //add one to fives counter
        break; //break statement for case 5
      case 6: //when numberCard4=6
        sixCounter++; //add one to sixs counter
        break; //break statement for case 6
      case 7: //when numberCard4=7
        sevenCounter++; //add one to sevens counter
        break; //break statement for case 7
      case 8: //when numberCard4=8
        eightCounter++; //add one to eights counter
        break; //break statement for case 8
      case 9: //when numberCard4=9
        nineCounter++; //add one to nines counter
        break; //break statement for case 9
      case 10: //when numberCard4=10
        tenCounter++; //add one to tens counter
        break; //break statement for case 10
      case 11: //when numberCard4=11
        jackCounter++; //add one to jack counter
        break; //break statement for case 11
      case 12: //when numberCard4=12
        queenCounter++; //add one to queen counter
        break; //break statement for case 12
    } //end switch statement for card 4
    switch (numberCard5) { //switch statement for card 5 to add to type of card counter
      case 0: //when numberCard5=0
        kingCounter++; //add one to king counter
        break; //break statement for case 0
      case 1: //when numberCard5=1
        aceCounter++; //add one to ace counter
        break; //break statement for case  1
      case 2: //when numberCard5=2
        twoCounter++; //add one to twos counter
        break; //break statement for case 2
      case 3: //when numberCard5=3
        threeCounter++; //add one to threes counter
        break; //break statement for case 3
      case 4: //when numberCard5=4
        fourCounter++; //add one to fours counter
        break; //break statement for case 4
      case 5: //when numberCard5=5
        fiveCounter++; //add one to fives counter
        break; //break statement for case 5
      case 6: //when numberCard5=6
        sixCounter++; //add one to sixs counter
        break; //break statement for case 6
      case 7: //when numberCard5=7
        sevenCounter++; //add one to sevens counter
        break; //break statement for case 7
      case 8: //when numberCard5=8
        eightCounter++; //add one to eights counter
        break; //break statement for case 8
      case 9: //when numberCard5=9
        nineCounter++; //add one to nines counter
        break; //break statement for case 9
      case 10: //when numberCard5=10
        tenCounter++; //add one to tens counter
        break; //break statement for case 10
      case 11: //when numberCard5=11
        jackCounter++; //add one to jack counter
        break; //break statement for case 11
      case 12: //when numberCard5=12
        queenCounter++; //add one to queen counter
        break; //break statement for case 12
    } //end switch statement for card 5
    //if statement to determine if poker hand is a pair
    if (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2) {
      //nested if and else if statements to determine if hand contains two pairs
      if (aceCounter==2 && (twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end if statement for pair of ace and another pair
      else if (twoCounter==2 && (aceCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of twos and another pair
      else if (threeCounter==2 && (aceCounter==2 || twoCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of threes and another pair
      else if (fourCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of fours and another pair
      else if (fiveCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of fives and another pair
      else if (sixCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of sixes and another pair
      else if (sevenCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of sevens and another pair
      else if (eightCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of eights and another pair
      else if (nineCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of nines and another pair
      else if (tenCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || jackCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of tens and another pair
      else if (jackCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || queenCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of jacks and another pair
      else if (queenCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || kingCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of queens and another pair
      else if (kingCounter==2 && (aceCounter==2 || twoCounter==2 || threeCounter==2 || fourCounter==2 || fiveCounter==2 || sixCounter==2 || sevenCounter==2 || eightCounter==2 || nineCounter==2 || tenCounter==2 || jackCounter==2 || queenCounter==2)) {
        System.out.println("You have two pairs!"); //print to user that type of hand has two pairs
      } //end else if statement for pair of kings and another pair
      else { 
        System.out.println("You have a pair!"); //print to user that type of hand is a pair
      } //end else statement if there are no two pairs found       
    } //end outside if statement for pair
    //else if statement to determine if poker hand is a three of a kind
    else if (aceCounter==3 || twoCounter==3 || threeCounter==3 || fourCounter==3 || fiveCounter==3 || sixCounter==3 || sevenCounter==3 || eightCounter==3 || nineCounter==3 || tenCounter==3 || jackCounter==3 || queenCounter==3 || kingCounter==3) {
      System.out.println("You have a three of a kind!"); //print to user that type of hand is a three of a kind
    } //end else if statement for three of a kind
    else { //else statement for high card hand
      System.out.println("You have a high card hand!"); //print to user that type of hand is high card hand
    } //end else statement 
  }//end main method
}//end class