/*
Name: Taylor Gomez
Date: 2/12/19
CSE 002 HW Convert - ask user for measurement in meters and convert to inches 
*/
import java.util.Scanner; //import scanner class
public class Convert { //class 
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    //receive inputs from user
    System.out.print("Enter the distance in meters (up to two decimal places): "); //ask user for measurement in meters up to two decimal places
    double distanceMeters = myScanner.nextDouble(); //user's input is placed in declared variable (distance in meters)
    //convert meters to inches based on conversion of 1 meter = 39.3701 inches
    double distanceInches = distanceMeters*39.3701; //declare distance in inches variables and convert distance in meters variable value by multiplying conversion rate
    //print output variables 
    System.out.println(distanceMeters + " meters is " + distanceInches + " inches."); //print out value of meters inputted and its value in inches
  }//end main method
} //end class 
    	
    