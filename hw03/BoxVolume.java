/*
Name: Taylor Gomez
Date: 2/12/19
CSE 002 HW BoxVolume - ask user for dimensions of box and calculate volume of box 
*/
import java.util.Scanner; //import scanner class
public class BoxVolume { //class
  //main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //declare instance of scanner and call scanner constructor
    //receive inputs from user
    System.out.print("The width side of the box is: "); //ask user for width of box
    double boxWidth = myScanner.nextDouble(); //user's input is placed in declared variable (width of box)
    System.out.print("The length of the box is: "); //ask user for length of box
    double boxLength = myScanner.nextDouble(); //user's input is placed in declared variable (length of box)
    System.out.print("The height of the box is: "); //ask user for height of box
    double boxHeight = myScanner.nextDouble(); //user's input is placed in declared variable (height of box)
    //declare and assign value to intermediate and output variables
    double boxVolume = boxWidth*boxLength*boxHeight; //calculate volume of box by multiplying width by length by height 
    //print out output variables (volume of box)
    System.out.println("The volume inside the box is: " + boxVolume); //print out volume of box 
  }//end main method 
}//end class 